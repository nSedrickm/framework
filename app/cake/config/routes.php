<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
         // Any controller will interpret the variable as id run through view action.

    // Admin prefix
    Router::prefix('admin', function ($routes) {
        // Pages for each controller
        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
        $routes->connect('/:controller/edit/*', ['action' => 'edit']);
        $routes->connect('/:controller/add/*', ['action' => 'add']);
        $routes->connect('/:controller/delete/*', ['action' => 'delete']);
        $routes->connect('/:controller/*', ['action' => 'view']);

        // Dashboard
        $routes->connect('', ['controller' => 'Artifacts', 'action' => 'dashboard']);

        $routes->fallbacks('DashedRoute');
    });

    Router::prefix('editor', function ($routes) {
        // Because you are in the admin scope,
        // you do not need to include the /admin prefix
        // or the admin route element.
        $routes->fallbacks(DashedRoute::class);
    });

    // Add Prefix rule for Api
    Router::scope('/api',function ($routes) {
        $routes->setExtensions(['json','xml','csv']);
        $routes->resources('Artifacts',['path'=>'catalogue']);
    });


    // Routes for Journals.
    //$routes->connect('/journals', ['controller' => 'Journals', 'action' => 'index']);

    // Visualization routes
    $routes->connect('/AdvancedSearch/getUpdatedLineChartData', ['controller' => 'AdvancedSearch', 'action' => 'getUpdatedLineChartData']);
    $routes->connect('/stats/:table', ['controller' => 'Stats', 'action' => 'index'], ['pass' => ['table']]);
    $routes->connect('/viz/:action', ['controller' => 'Viz']);
    $routes->connect('/heatmap/post', ['controller' => 'Heatmap', 'action' => 'post']);

    // Covering absense of user add
    $routes->connect('/users/index', ['controller' => 'Users', 'action' => 'index']);
    $routes->connect('/users/add', ['controller' => 'Users', 'action' => 'index']);

    // Pages for each controller
    $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
    $routes->connect('/:controller/edit/*', ['action' => 'edit']);
    $routes->connect('/:controller/add/*', ['action' => 'add']);
    $routes->connect('/:controller/delete/*', ['action' => 'delete']);
    $routes->connect('/:controller/*', ['action' => 'view']);

    // Home page
    $routes->connect('/', ['controller' => 'Search']);

    // Browse
    $routes->connect('/browse', ['controller' => 'Artifacts', 'action' => 'browse']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
    */
    //Set up router for REST API
    $routes->resources('Artifacts');

    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */

Plugin::routes();
