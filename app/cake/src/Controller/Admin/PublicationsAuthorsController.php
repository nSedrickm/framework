<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * PublicationsAuthors Controller
 *
 * @property \App\Model\Table\PublicationsAuthorsTable $PublicationsAuthors
 *
 * @method \App\Model\Entity\PublicationsAuthor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicationsAuthorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Publications', 'Authors']
        ];
        $publicationsAuthors = $this->paginate($this->PublicationsAuthors);

        $this->set(compact('publicationsAuthors'));
    }

    /**
     * View method
     *
     * @param string|null $id Publications Author id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $publicationsAuthor = $this->PublicationsAuthors->get($id, [
            'contain' => ['Publications', 'Authors']
        ]);

        $this->set('publicationsAuthor', $publicationsAuthor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $publicationsAuthor = $this->PublicationsAuthors->newEntity();
        if ($this->request->is('post')) {
            $publicationsAuthor = $this->PublicationsAuthors->patchEntity($publicationsAuthor, $this->request->getData());
            if ($this->PublicationsAuthors->save($publicationsAuthor)) {
                $this->Flash->success(__('The publications author has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The publications author could not be saved. Please, try again.'));
        }
        $publications = $this->PublicationsAuthors->Publications->find('list', ['limit' => 200]);
        $authors = $this->PublicationsAuthors->Authors->find('list', ['limit' => 200]);
        $this->set(compact('publicationsAuthor', 'publications', 'authors'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Publications Author id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $publicationsAuthor = $this->PublicationsAuthors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $publicationsAuthor = $this->PublicationsAuthors->patchEntity($publicationsAuthor, $this->request->getData());
            if ($this->PublicationsAuthors->save($publicationsAuthor)) {
                $this->Flash->success(__('The publications author has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The publications author could not be saved. Please, try again.'));
        }
        $publications = $this->PublicationsAuthors->Publications->find('list', ['limit' => 200]);
        $authors = $this->PublicationsAuthors->Authors->find('list', ['limit' => 200]);
        $this->set(compact('publicationsAuthor', 'publications', 'authors'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Publications Author id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $publicationsAuthor = $this->PublicationsAuthors->get($id);
        if ($this->PublicationsAuthors->delete($publicationsAuthor)) {
            $this->Flash->success(__('The publications author has been deleted.'));
        } else {
            $this->Flash->error(__('The publications author could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
