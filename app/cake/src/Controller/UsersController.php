<?php
namespace App\Controller;

use App\Controller\AppController;
use GoogleAuthenticator\GoogleAuthenticator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->deny(['view']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id);
        $this->set('user', $user);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $id = $this->Auth->user('id');
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $authors = $this->Users->Authors->find('list', ['limit' => 200]);
        $this->set(compact('user', 'authors'));
    }

    /**
     * Index method
    **/
    public function index()
    {
        $this->loadComponent('GoogleAuthenticator');
        $ga = $this->GoogleAuthenticator;

        //check if already login
        $id = $this->Auth->user('id');

        if ($id) {
        //redirect to view
            return $this->redirect([
                'controller' => 'Users',
                'action' => 'view',
                $id
            ]);
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            //check if login available
            if ($user) {
                $session = $this->getRequest()->getSession();
                $session->write('User', $user);
                $twofa_status = $user['2fa_status'];

                //check if user have 2fa
                if ($twofa_status) {
                    return $this->redirect([
                        'controller' => 'Twofactorlogin',
                        'action' => 'index',
                    ]);
                }
                else {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }
            }
            else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
    }

}
