<div id="viz-container">
    <div class="container-fluid">
        <div id="radar-chart">
            <!--Viz Goes here-->
        </div>
        
        <!--Fallback Image-->
        <noscript>
            <?= $this->Html->image('d3-fallbacks/radar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Radar Chart']) ?>
            <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
        </noscript>
    </div>
</div>

<script type="text/javascript">
    var data = <?= $data ?>;
    var legendOptions = <?= $legendOptions ?>;
</script>

<?php echo $this->Html->script(['d3', 'd3-radar']); ?>