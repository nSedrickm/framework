<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCollection $artifactsCollection
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Artifacts Collection') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsCollection->has('artifact') ? $this->Html->link($artifactsCollection->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsCollection->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Collection') ?></th>
                    <td><?= $artifactsCollection->has('collection') ? $this->Html->link($artifactsCollection->collection->id, ['controller' => 'Collections', 'action' => 'view', $artifactsCollection->collection->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsCollection->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Artifacts Collection'), ['action' => 'edit', $artifactsCollection->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Artifacts Collection'), ['action' => 'delete', $artifactsCollection->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsCollection->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts Collections'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Collection'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



