<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PublicationsAuthor $publicationsAuthor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Publications Author'), ['action' => 'edit', $publicationsAuthor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Publications Author'), ['action' => 'delete', $publicationsAuthor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $publicationsAuthor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Publications Authors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Publications Author'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="publicationsAuthors view large-9 medium-8 columns content">
    <h3><?= h($publicationsAuthor->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Publication') ?></th>
            <td><?= $publicationsAuthor->has('publication') ? $this->Html->link($publicationsAuthor->publication->title, ['controller' => 'Publications', 'action' => 'view', $publicationsAuthor->publication->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Author') ?></th>
            <td><?= $publicationsAuthor->has('author') ? $this->Html->link($publicationsAuthor->author->id, ['controller' => 'Authors', 'action' => 'view', $publicationsAuthor->author->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($publicationsAuthor->id) ?></td>
        </tr>
    </table>
</div>
