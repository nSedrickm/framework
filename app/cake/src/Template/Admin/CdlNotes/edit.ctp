<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CdlNote $cdlNote
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($cdlNote) ?>
            <legend class="capital-heading"><?= __('Edit Cdl Note') ?></legend>
            <?php
                echo $this->Form->control('year');
                echo $this->Form->control('number');
                echo $this->Form->control('author_id', ['options' => $authors, 'empty' => true]);
                echo $this->Form->control('title');
                echo $this->Form->control('text');
                echo $this->Form->control('footnotes');
                echo $this->Form->control('bibliography');
                echo $this->Form->control('uploaded', ['empty' => true]);
                echo $this->Form->control('preprint', ['empty' => true]);
                echo $this->Form->control('archival', ['empty' => true]);
                echo $this->Form->control('preprint_state');
                echo $this->Form->control('online');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cdlNote->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cdlNote->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Cdl Notes'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
