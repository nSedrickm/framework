<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialColor $materialColor
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($materialColor) ?>
            <legend class="capital-heading"><?= __('Edit Material Color') ?></legend>
            <?php
                echo $this->Form->control('material_color');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $materialColor->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $materialColor->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Material Colors'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts Materials'), ['controller' => 'ArtifactsMaterials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Material'), ['controller' => 'ArtifactsMaterials', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
