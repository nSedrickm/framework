<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading text-center">Register</div>
        <?= $this->Flash->render() ?>

        <?= $this->Form->create() ?>
            <?= $this->Form->control('email') ?>
            <?= $this->Form->control('username') ?>
            <?= $this->Form->control('password') ?>

            <?= $this->Form->submit(); ?>
        <?= $this->Form->end() ?>
    </div>

</div>
