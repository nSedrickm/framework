<div>
    <h3><?= __('Search')?></h3>
	<?= $this->Form->create("",['type'=>'get']) ?>

<!-- 			<?= $this->Form->control('Publications'); ?>
	<?= $this->Form->control('Collections'); ?>
	<?= $this->Form->control('Proveniences'); ?>
	<?= $this->Form->control('Periods'); ?>
	<?= $this->Form->control('Transliteraton'); ?>
	<?= $this->Form->control('CDLI'); ?> -->

<!-- 	<?php foreach ($result as $row)
	{
		echo "<br /> " . $row;
	}
	?> -->

        <div class="table-responsive">
            <table class="table table-bordered" id="dynamic_field">
                <tr>
                    <td>		
                        <div class="input-group">
							<label hidden for="input1">Query</label>
                            <input type="text" class="form-control name_list border-top-right border-bottom-right" id="input1" name="Publications" placeholder="" aria-label="" aria-describedby="">
                            
                            <div class="input-group-append">
								<label hidden for="1">Category</label>
                                <select class="form-group custom-select mb-0" id="1">
                                    <option value="Publications1">Publications</option>
                                    <option value="Collections1">Collections</option>
                                    <option value="Proveniences1">Proveniences</option>
                                    <option value="Periods1">Periods</option>
                                    <option value="Transliteraton1">Transliteraton</option>
                                    <option value="CDLI1">CDLI no</option>
                                </select>
                            </div>
                        </div>                        
                    </td>
                    <td><button type="button" name="add" id="add" class="btn btn-success">Add</button></td>
                </tr>
            </table>
        </div>
        <?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'btn btn-info']); ?>
	<?= $this->Form->end()?>
</div>
<?php if(!empty($result)){?>
	<?php foreach ($result as $row) {?>

		<table>
			<tr>
				<th rowspan="10"> 
					<?php echo $row["id"];?>
					<?=
					 $this->Html->link(
						'Showmore',
						'showmore?CDLI='.$row["id"].'&Publication='.$row["_matchingData"]["Publications"]["id"] ,
						['class' => 'btn btn-primary btn-lg active"', 'target' => '_blank']
					)
					?>
				</th>
				<th > 
					<?php echo $row["_matchingData"]["Publications"]["designation"];?>
				</th>
			</tr>
			<tr>
				<td >
					<?php echo "Author: ".$row["_matchingData"]["Authors"]["author"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Publications Date: ".$row["_matchingData"]["Publications"]["year"]; ?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "CDLI No.: ".$row["id"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Collection: ".$row["_matchingData"]["Collections"]["collection"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Provenience: ".$row["_matchingData"]["Proveniences"]["provenience"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Period: ".$row["_matchingData"]["Periods"]["period"];?>
				</td>
			</tr>		
			<tr>
				<td >
					<?php echo "Object Type: ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Material: ".$row["_matchingData"]["Materials"]["material"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Transliteration/Translation: ".$row["_matchingData"]["Inscriptions"]["transliteration"];?>
				</td>
			</tr>
		</table>

	<?php }?>
<?php }?>

<script>
	$(document).ready(function(){
		var i=1;
		$('#add').click(function(){
			i++;
			$('#dynamic_field').append('<tr id="row'+i+'"> <td> <div class="input-group"> <input type="text" class="form-control name_list border-top-right border-bottom-right" id="input'+i+'"/> <div class="input-group-append"> <select id="'+i+'" class="form-group custom-select mb-0"> <option value="Publications'+i+'">Publications</option> <option value="Collections'+i+'">Collections</option> <option value="Proveniences'+i+'">Proveniences</option> <option value="Periods'+i+'">Periods</option> <option value="Transliteraton'+i+'">Transliteraton</option> <option value="CDLI'+i+'">CDLI no</option> </div> </div> </td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td> </tr>');
		});
        
		$(document).on('click', '.btn_remove', function(){
			var button_id = $(this).attr("id"); 
			$('#row'+button_id+'').remove();
		});

		$(document).on('change', '.form-group', function(){
			var select_id = $(this).attr("id"); 
			var select_name = $(this).children("option:selected").val();; 
			$('#input'+select_id).attr('name', select_name);
		});

		// $( ".form-group").change(function() {
		// 	var select_id = $(this).attr("id"); 
		// 	var select_name = $(this).children("option:selected").val();; 
		// 	$('#input'+select_id).attr('name', select_name);
		// });

	});
</script>
