<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Credits Model
 *
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsTo $Authors
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\HasMany $Artifacts
 *
 * @method \App\Model\Entity\Credit get($primaryKey, $options = [])
 * @method \App\Model\Entity\Credit newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Credit[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Credit|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Credit|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Credit patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Credit[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Credit findOrCreate($search, callable $callback = null, $options = [])
 */
class CreditsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('credits');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Authors', [
            'foreignKey' => 'author_id'
        ]);
        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id'
        ]);
        $this->hasMany('Artifacts', [
            'foreignKey' => 'credit_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->date('date')
            ->allowEmpty('date');

        $validator
            ->scalar('comments')
            ->maxLength('comments', 100)
            ->allowEmpty('comments');

        $validator
            ->scalar('credit_to')
            ->maxLength('credit_to', 30)
            ->allowEmpty('credit_to');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['author_id'], 'Authors'));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts'));

        return $rules;
    }
}
