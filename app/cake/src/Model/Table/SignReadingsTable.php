<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SignReadings Model
 *
 * @property \App\Model\Table\PeriodsTable&\Cake\ORM\Association\BelongsTo $Periods
 * @property \App\Model\Table\ProveniencesTable&\Cake\ORM\Association\BelongsTo $Proveniences
 * @property \App\Model\Table\LanguagesTable&\Cake\ORM\Association\BelongsTo $Languages
 * @property \App\Model\Table\SignReadingsCommentsTable&\Cake\ORM\Association\HasMany $SignReadingsComments
 *
 * @method \App\Model\Entity\SignReading get($primaryKey, $options = [])
 * @method \App\Model\Entity\SignReading newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SignReading[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SignReading|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SignReading saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SignReading patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SignReading[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SignReading findOrCreate($search, callable $callback = null, $options = [])
 */
class SignReadingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('sign_readings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Periods', [
            'foreignKey' => 'period_id',
        ]);
        $this->belongsTo('Proveniences', [
            'foreignKey' => 'provenience_id',
        ]);
        $this->belongsTo('Languages', [
            'foreignKey' => 'language_id',
        ]);
        $this->hasMany('SignReadingsComments', [
            'foreignKey' => 'sign_reading_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', null, 'create');

        $validator
            ->scalar('sign_name')
            ->maxLength('sign_name', 255)
            ->allowEmpty('sign_name');

        $validator
            ->scalar('sign_reading')
            ->maxLength('sign_reading', 255)
            ->allowEmpty('sign_reading');

        $validator
            ->boolean('preferred_reading')
            ->allowEmpty('preferred_reading');

        $validator
            ->scalar('meaning')
            ->maxLength('meaning', 255)
            ->allowEmpty('meaning');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['period_id'], 'Periods'));
        $rules->add($rules->existsIn(['provenience_id'], 'Proveniences'));
        $rules->add($rules->existsIn(['language_id'], 'Languages'));

        return $rules;
    }
}
