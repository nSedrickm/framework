<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Publication Entity
 *
 * @property int $id
 * @property string|null $bibtexkey
 * @property string|null $year
 * @property int|null $entry_type_id
 * @property string|null $address
 * @property string|null $annote
 * @property string|null $book_title
 * @property string|null $chapter
 * @property string|null $crossref
 * @property string|null $edition
 * @property string|null $editor
 * @property string|null $how_published
 * @property string|null $institution
 * @property int|null $journal_id
 * @property string|null $month
 * @property string|null $note
 * @property string|null $number
 * @property string|null $organization
 * @property string|null $pages
 * @property string|null $publisher
 * @property string|null $school
 * @property string|null $title
 * @property string|null $volume
 * @property string|null $publication_history
 * @property int|null $abbreviation_id
 * @property string|null $series
 * @property int|null $oclc
 * @property string|null $designation
 *
 * @property \App\Model\Entity\EntryType $entry_type
 * @property \App\Model\Entity\Journal $journal
 * @property \App\Model\Entity\Abbreviation $abbreviation
 * @property \App\Model\Entity\Artifact[] $artifacts
 * @property \App\Model\Entity\Author[] $authors
 */
class Publication extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'bibtexkey' => true,
        'year' => true,
        'entry_type_id' => true,
        'address' => true,
        'annote' => true,
        'book_title' => true,
        'chapter' => true,
        'crossref' => true,
        'edition' => true,
        'editor' => true,
        'how_published' => true,
        'institution' => true,
        'journal_id' => true,
        'month' => true,
        'note' => true,
        'number' => true,
        'organization' => true,
        'pages' => true,
        'publisher' => true,
        'school' => true,
        'title' => true,
        'volume' => true,
        'publication_history' => true,
        'abbreviation_id' => true,
        'series' => true,
        'oclc' => true,
        'designation' => true,
        'entry_type' => true,
        'journal' => true,
        'abbreviation' => true,
        'artifacts' => true,
        'authors' => true
    ];
}
