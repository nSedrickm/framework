<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SignReadingsComment Entity
 *
 * @property int $id
 * @property int $sign_reading_id
 * @property string $comment
 * @property \Cake\I18n\FrozenDate $date
 * @property int|null $author_id
 *
 * @property \App\Model\Entity\SignReading $sign_reading
 * @property \App\Model\Entity\Author $author
 */
class SignReadingsComment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sign_reading_id' => true,
        'comment' => true,
        'date' => true,
        'author_id' => true,
        'sign_reading' => true,
        'author' => true,
    ];
}
