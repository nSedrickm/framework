<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Posting Entity
 *
 * @property int $id
 * @property int|null $posting_type_id
 * @property string|null $slug
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property bool|null $published
 * @property string|null $title
 * @property string|null $body
 * @property string|null $lang
 * @property int|null $created_by
 * @property string|null $modified_by
 * @property \Cake\I18n\FrozenTime|null $publish_start
 * @property \Cake\I18n\FrozenTime|null $publish_end
 *
 * @property \App\Model\Entity\PostingType $posting_type
 */
class Posting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'posting_type_id' => true,
        'slug' => true,
        'created' => true,
        'modified' => true,
        'published' => true,
        'title' => true,
        'body' => true,
        'lang' => true,
        'created_by' => true,
        'modified_by' => true,
        'publish_start' => true,
        'publish_end' => true,
        'posting_type' => true
    ];
}
